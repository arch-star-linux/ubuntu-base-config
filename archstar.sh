sudo apt update -y &&
sudo apt upgrade -y  &&
sudo apt install git &&
cd ~ &&
git clone https://gitlab.com/arch-star-linux/ubuntu-base-config &&
sudo apt install chromium spectral tilda catfish copyq compton conky curl dex nitrogen flameshot git-lfs gparted htop i3 inkscape lxappearance meld mpv ntfs-3g qbittorrent ranger rofi spectrwm tmux tmate timeshift thunar rar vim zsh xfce4-panel xfce4-pulseaudio-plugin xfce4-session xfce4-settings xfce4-wavelan-plugin xfce4-systemload-plugin xfce4-power-manager xfwm4 xfce4-whiskermenu-plugin xscreensaver xterm zip zsh -y  && 
sudo cp -rf ~/ubuntu-base-config/skel/.config/* ~/.config/ &&
sudo cp -rf ~/ubuntu-base-config/skel/.gtkrc-2.0 ~/ &&
sudo cp -rf ~/ubuntu-base-config/skel/.zshrc ~/ &&
sudo cp -rf ~/ubuntu-base-config/skel/.bashrc ~/ &&
sudo cp -rf ~/ubuntu-base-config/usr/local/share/* /usr/local/share/*  &&
#nitrogen --set-scaled wave.svg &&
sudo apt install ./cudatext_1.155.3.1-1_gtk2_amd64.deb
