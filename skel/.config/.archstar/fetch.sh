host="$(hostnamectl --static)"
os='Arch Star'
kernel="$(uname -sr)"
uptime="$(uptime -p | sed 's/up //')"
packages="$(apt -Q | wc -l)"
shell="$(basename ${SHELL})"
 
if [ -z "${WM}" ]; then
    if [ "${XDG_CURRENT_DESKTOP}" ]; then
        envtype='DE'
        WM="${XDG_CURRENT_DESKTOP}"
    elif [ "${DESKTOP_SESSION}" ]; then
        envtype='DE'
        WM="${DESKTOP_SESSION}"
    else
        envtype='WM'
        WM="$(tail -n 1 "${HOME}/.xinitrc" | cut -d ' ' -f 2)"
    fi
else
    envtype='WM'
fi
 
## DEFINE COLORS
 
# probably don't change these
bold="$(tput bold)"
black="$(tput setaf 0)"
red="$(tput setaf 1)"
green="$(tput setaf 2)"
yellow="$(tput setaf 3)"
blue="$(tput setaf 4)"
magenta="$(tput setaf 5)"
cyan="$(tput setaf 6)"
white="$(tput setaf 7)"
reset="$(tput sgr0)"
 
# you can change these
lc="${reset}${bold}${cyan}"     # labels
nc="${reset}${bold}${cyan}"     # user and hostname
ic="${reset}${bold}${white}"    # info
c0="${reset}${bold}${cyan}"     # first color
c1="${reset}${cyan}"            # second color
 
## OUTPUT

cat<<EOF
${c0}  xxxxxxxxxxxxxxxxOxxxxxxxxxxxxxxxxx
${c1}  XXXXXXXXXXXXXXXx..cXXXXXXXXXXXXXXX
${c0}  XXXXXXXXXXXXXXx''.'oXXXXXXXXXXXXXX
${c1}  XXXXXXXXXXXXXx','.,'lXXXXXXXXXXXXX 
${c0}  XXXXXXXXXXXXo,,,'.,,'cKXXXXXXXXXXX  ${nc}${USER}${ic}@${nc}${host}${reset}
${c1}  XKdloddxxkOc,,,,'.,,,,:kkkxddolo0X  ${lc}OS:        Arch Star
${c0}  XXXd       .,,,,'.,,,,,.     .oKXX  ${lc}KERNEL:    ${ic}${kernel}${reset}
${c1}  XXXXXk,   .,,,,,'',,,,,,.   xXXXXX  ${lc}UPTIME:    ${ic}${uptime}${reset}
${c0}  XXXXXXXl .,,,,,''.',,,,,,.;XXXXXXX  ${lc}PACKAGES:  ${ic}${packages}${reset}
${c1}  XXXXXXd',,,,,' cMx .,,,,,'lXXXXXXX  ${lc}SHELL:     ${ic}${shell}${reset}
${c0}  XXXXXd',,,,,   lMO   ,,,,,'lXXXXXX  ${lc}${envtype}:        ${ic}${WM}${reset}
${c1}  XXXXl',,,'     .N:.    ',,,'cKXXXX
${c0}  XXK:,,co'    .o0XXKd,    .oc,';KXX
${c1}  XXddOKXk   o0dkodx0kOlo   lXXOxoXX
${c0}  XXXXXXK. XXXXXXXXXXXXXXXX .0XXXXXX
${c1}  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
